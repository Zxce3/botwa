let handler = async (m) => {
    let totalreg = Object.keys(global.db.data.users).length
    let chattotal = Object.keys(global.db.data.chats).length
    let rtotalreg = Object.values(global.db.data.users).filter(user => user.registered == true).length
    m.reply(`
    *Total chat ${chattotal}*
    *Jumlah database saat ini ${totalreg} user*
    *Jumlah terdaftar ${rtotalreg} user*
    `)
}
handler.help = ['database', 'user']
handler.tags = ['info']
handler.command = /^(database|jumlahdatabase|user)$/i
module.exports = handler
